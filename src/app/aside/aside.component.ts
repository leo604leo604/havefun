import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  constructor() { }
  data: any[] = [{
    value: 'Taiper',
    label: '台北',
  }, {
    value: 'shizitou',
    label: '台中',
  }, {
    value: 'luosifen',
    label: '台南',
  }, {
    value: 'luosifen',
    label: '高雄',
  }
];
  ngOnInit() {
  }

}
