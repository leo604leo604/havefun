import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }
  tags: any[] = [
    {
      name: '台北'
    }, {
      name: '台中'
    }, {
      name: '台南'
    }, {
      name: '高雄'
    }];
  ngOnInit() {
  }

}
